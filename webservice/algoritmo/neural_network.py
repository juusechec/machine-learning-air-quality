import numpy as np
import pandas as pd
from joblib import load

def get_rms(signal):
    return (np.sqrt(np.mean((signal[0]*0.002) ** 2)))/3.4

def get_variance(signal):
    return np.var(signal * 0.002)

def neural_network(coordenada):
    xmax = -74.0097849999999937
    xmin = -74.2235809999999958

    ymax = 4.8306610000000001
    ymin = 4.4578600000000002

    # μg/m3
    minval = 0
    maxval = 250


    x = coordenada[0]
    y = coordenada[1]

    normalized_x = (x - xmin) / (xmax - xmin)
    normalized_y = (y - ymin) / (ymax - ymin)

    columns = [
        'x',
        'y',
    ]
    data = [
        normalized_x,
        normalized_y
    ]
    # print('data', data)
    X = pd.DataFrame([data], columns=columns)
    print('dataframe', X.transpose())
    mlp = load('./algoritmo/resources/MLPRegressorbest1556209999110.joblib')
    y_pred = mlp.predict(X)
    print('y_pred', y_pred)
    unnormalized_y_pred = (y_pred*(maxval - minval)) + minval
    print('unnormalized_y_pred', unnormalized_y_pred)
    return unnormalized_y_pred