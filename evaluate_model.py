#!/usr/bin/env python3

import time
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from random import randint
from joblib import load

gdf = gpd.read_file('data/ConcentracionMensual25.gpkg', layer='PuntosConcentracionMensual25')

xmax=-74.0097849999999937
xmin=-74.2235809999999958

ymax=4.8306610000000001
ymin=4.4578600000000002

# μg/m3
minval=0
maxval=250

# normalizar
gdf['x'] = gdf.apply(lambda row: (row['geometry'].x-xmin)/(xmax-xmin), axis=1)
gdf['y'] = gdf.apply(lambda row: (row['geometry'].y-ymin)/(ymax-ymin), axis=1)
gdf['valor'] = gdf.apply(lambda row: (row['valor']-minval)/(maxval-minval), axis=1)
# print(min(gdf['x']))
# print(max(gdf['x']))
# print(min(gdf['y']))
# print(max(gdf['y']))
# print(min(gdf['valor']))
# print(max(gdf['valor']))

X = gdf.as_matrix( columns = [
    'x',
    'y'
])
y = gdf['valor']

cor=gdf.corr()
print(gdf.corr())

mlp = load('MLPRegressorbest1556209999110.joblib')
y_pred = mlp.predict(X)

params = mlp.get_params()
print('Params:', params)

mse = mean_squared_error(y, y_pred)
rmse = np.sqrt(mean_squared_error(y, y_pred))
mae = mean_absolute_error(y, y_pred)
rmae = np.sqrt(mean_absolute_error(y, y_pred))
r2 = r2_score(y, y_pred)

print(u'Error cuadrático medio: {:.10f}'.format(mse))
print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
print(u'Raiz Error absoluto medio: %.2f' % rmae)
print(u'Estadístico R_2: %.10f' % r2)


#sorted_test = y.sort_values()
#sorted_pred = pd.Series(y_pred)[sorted_test.index]
#plt.plot(sorted_test.values*600, color='b')
#plt.plot(sorted_pred.values*600, color='r')
est=np.linspace(0, 70, 70)
unnormalized_y = (y*(maxval - minval)) + minval
unnormalized_y_pred = (y_pred*(maxval - minval)) + minval
plt.scatter(unnormalized_y, unnormalized_y_pred, marker='o',c='w', edgecolor='b')
plt.plot(est, est, c='r')
plt.plot(est, est+4.275738925, c='g', linestyle='--')
plt.plot(est, est-4.275738925, c='g', linestyle='--')
plt.grid(True)
plt.xlim(0, 70)
plt.ylim(0, 70)
plt.xlabel('PM_2.5 medida / μg/m3')
plt.ylabel('PM_2.5 estimada / μg/m3')
plt.show()