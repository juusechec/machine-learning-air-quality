#!/usr/bin/env python3
import time
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from random import randint
from joblib import dump

gdf = gpd.read_file('data/ConcentracionMensual25.gpkg', layer='PuntosConcentracionMensual25')

xmax=-74.0097849999999937
xmin=-74.2235809999999958

ymax=4.8306610000000001
ymin=4.4578600000000002

# μg/m3
minval=0
maxval=250

# normalizar
gdf['x'] = gdf.apply(lambda row: (row['geometry'].x-xmin)/(xmax-xmin), axis=1)
gdf['y'] = gdf.apply(lambda row: (row['geometry'].y-ymin)/(ymax-ymin), axis=1)
gdf['valor'] = gdf.apply(lambda row: (row['valor']-minval)/(maxval-minval), axis=1)
# print(min(gdf['x']))
# print(max(gdf['x']))
# print(min(gdf['y']))
# print(max(gdf['y']))
# print(min(gdf['valor']))
# print(max(gdf['valor']))

X = gdf.as_matrix( columns = [
    'x',
    'y'
])
y = gdf['valor']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True)

def create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, fxActivacion):
    capas = {
        1: (neuronasCapa1),
        2: (neuronasCapa1, neuronasCapa2),
        3: (neuronasCapa1, neuronasCapa2, neuronasCapa3),
        4: (neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4),
    }

    mlp = MLPRegressor(
        hidden_layer_sizes=capas[numCapas],
        max_iter=50000000,
        activation=fxActivacion,
        shuffle=True,
        solver='lbfgs'
    )

    mlp.fit(X_train, y_train)

    y_pred = mlp.predict(X)

    mse = mean_squared_error(y, y_pred)
    rmse = np.sqrt(mean_squared_error(y, y_pred))
    mae = mean_absolute_error(y, y_pred)
    rmae = np.sqrt(mean_absolute_error(y, y_pred))
    r2 = r2_score(y, y_pred)

    print(u'Error cuadrático medio: {:.10f}'.format(mse))
    print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
    print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
    print(u'Raiz Error absoluto medio: %.2f' % rmae)
    print(u'Estadístico R_2: %.10f' % r2)

    params = mlp.get_params()
    print('Params:', params)
    # print('len coef:', len(mlp.coefs_))
    # print('len coef[0]:', len(mlp.coefs_[0]))
    # print('coefs:', mlp.coefs_)
    # print('len intercepts:', len(mlp.intercepts_))
    # print('len intercepts[0]:', len(mlp.intercepts_[0]))
    # print('intercepts:', len(mlp.intercepts_))

    data = {
        'mse': mse,
        'rmse': rmse,
        'mae': mae,
        'rmae': rmae,
        'r2': r2,
        'params': params,
        'coef': mlp.coefs_,
        'intercepts': mlp.intercepts_,
        'y_pred': y_pred
    }
    return rmse, data, mlp

error_deseado = 0.017 # μg/m3
error_obtenido = 10000 # numero inicial grande
data = None
mlp = None
while error_obtenido >= error_deseado:
    numCapas = randint(1, 4)
    neuronasCapa1 = randint(1, 4)
    neuronasCapa2 = randint(1, 4)
    neuronasCapa3 = randint(1, 4)
    neuronasCapa4 = randint(1, 4)
    indexActivation = randint(0, 3)
    activacion = ('identity', 'logistic', 'tanh', 'relu')
    error_obtenido, data, mlp = create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, activacion[indexActivation])

millis = int(round(time.time() * 1000))
dump(mlp, 'MLPRegressorbest{}.joblib'.format(millis))
print('Datos finales:', data)
print('error_obtenido final', error_obtenido)
plt.plot(y, color='r')
plt.plot(data['y_pred'], color='b')
plt.show()
